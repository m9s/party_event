# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    events = fields.Many2Many('party.event-party.party', 'party', 'event',
        'Events', context={'party': Eval('active_id')})

Party()
