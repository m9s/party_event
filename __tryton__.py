# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Party Event',
    'name_de_DE': 'Partei Ereignisse',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Allows to track events related to parties.''',
    'description_de_DE': '''Ermöglicht die Erfassung von parteienbezogenen
    Ereignissen''',
    'depends': [
        'party',
    ],
    'xml': [
        'event.xml',
        'party.xml',
        'configuration.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
